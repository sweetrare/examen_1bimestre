package com.example.examen_1erbimestre

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation.findNavController

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [mainFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [mainFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class mainFragment : Fragment() {
    // TODO: Rename and change types of parameters

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view: View = inflater.inflate(R.layout.fragment_main, container, false)

        val btnSumar = view.findViewById<Button>(R.id.btnSumar)
        btnSumar.setOnClickListener {
            findNavController(view).navigate(R.id.toSumas)
        }

        val btnRestar = view.findViewById<Button>(R.id.btnRestar)
        btnRestar.setOnClickListener {
            findNavController(view).navigate(R.id.toResta)
        }

        val btnMultiplicar = view.findViewById<Button>(R.id.btnMultiplicar)
        btnMultiplicar.setOnClickListener {
            findNavController(view).navigate(R.id.toMultiplicar)
        }


        val btnDividir = view.findViewById<Button>(R.id.btnDividir)
        btnDividir.setOnClickListener {
            findNavController(view).navigate(R.id.toDividir)
        }

        return view
    }

    // TODO: Rename method, update argument and hook method into UI event

}
