package com.example.examen_1erbimestre

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random


class multiplicarFragment : Fragment() {
    private  lateinit var txtTiempo: TextView
    private  lateinit var txtScore: TextView
    private lateinit var txtRound: TextView
    private lateinit var countDownTimer: CountDownTimer //implementa el Timer.
    var timeLeft = 10

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000

    // TODO: Rename and change types of parameters

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sumas, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val txtSigno = view.findViewById(R.id.txtSigno) as TextView
        txtSigno.text = "X"

        val primerNumero = Random.nextInt(0, 20)
        val segundoNumero = Random.nextInt(0, 20)
        val txtPrimerNumero = view.findViewById(R.id.txtPrimerNumero) as TextView
        txtPrimerNumero.text = primerNumero.toString()

        restoreGame()

        val txtSegundoNumero = view.findViewById(R.id.txtSegundoNUmero) as TextView
        txtSegundoNumero.text = segundoNumero.toString()

        txtTiempo = view.findViewById(R.id.txtTiempo) as TextView
        txtScore = view.findViewById(R.id.txtScore) as TextView
        txtScore.text = "0"
        txtRound = view.findViewById(R.id.txtRound) as TextView
        txtRound.text = "1"

        val btnSend = view.findViewById(R.id.btnSend) as Button
        btnSend.setOnClickListener {

            val numeroIngresado = view.findViewById(R.id.txtRespuesta) as EditText

            Log.e("hola", numeroIngresado.text.toString() )

            if ( numeroIngresado.text.toString().toInt() == primerNumero * segundoNumero )
            {

                var score: Int = 0


                when (txtTiempo.text.toString().toInt()) {
                    in 0..4 -> score = 10
                    in 5..7 -> score = 50
                    in 8..10 -> score = 100
                }
                Toast.makeText(activity, "Bien tu puntuacion es:"+ score.toString(), Toast.LENGTH_LONG).show()
                txtScore.text = score.toString()
            }
            else {
                Toast.makeText(activity, "Mal", Toast.LENGTH_LONG).show()
            }

            txtRound.text = (txtRound.text.toString().toInt() + 1).toString()

        }

    }


    private fun restoreGame() {

        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval) {
            override fun onFinish() {

                txtRound.text = (txtRound.text.toString().toInt() + 1).toString()

            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000

                txtTiempo.text = timeLeft.toString()
            }

        }

        countDownTimer.start()
    }


}
